package ch.ffhs.ftoop.p1.producerconsumer;

public class Speicher implements SpeicherIf {

	private int wert;
	private boolean hatWert = false;

	/**
	 * Gibt den aktuellen Wert zurueck.
	 * 
	 * @return
	 * @throws InterruptedException
	 */
	@Override
	public synchronized int getWert() throws InterruptedException {
		hatWert = false;
		return wert;
	}

	/**
	 * Setzt einen neuen aktuellen Wert.
	 * 
	 * @param wert
	 * @throws InterruptedException
	 */
	@Override
	public synchronized void setWert(int wert) throws InterruptedException {
		this.wert = wert;
		hatWert = true;
	}

	/**
	 * Gibt true zurueck, wenn es einen neuen, noch nicht konsumierten Wert im
	 * Objekt hat.
	 * 
	 * @return
	 */
	@Override
	public synchronized boolean isHatWert() {
		return hatWert;
	}
}
