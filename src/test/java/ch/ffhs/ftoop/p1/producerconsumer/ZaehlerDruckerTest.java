package ch.ffhs.ftoop.p1.producerconsumer;

import org.junit.Test;

import student.TestCase;

/**
 * This a test class to test the ZaehlerDrucker class. It inheritants from the TestCase class.  
 */

public class ZaehlerDruckerTest extends TestCase {

	/**
	 * This method tests the ZaehlerDrucker class. It compares the output from the Drucker instance to the String which is specified in the assertFuzzyEquals method.
	 * @throws InterruptedException
	 */
	
	public void testZaehlerDrucker() throws InterruptedException {
		ZaehlerDrucker.main(new String[] { "1", "25" });
		assertFuzzyEquals(
				"1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 ",
				systemOut().getHistory());
	}
}