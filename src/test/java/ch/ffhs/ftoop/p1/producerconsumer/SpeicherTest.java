package ch.ffhs.ftoop.p1.producerconsumer;

import java.util.LinkedList;

import junit.framework.TestCase;

/**
 * 
 * This is a test class to test the Speicher class. 
 * 
 */

public class SpeicherTest extends TestCase {
		
		/**
		 * Tests the method IsHatWert(). Creates a new Speicher instance, inputs a value (24) in the Speicher instance s1 and test if the method isHatWert returns true.
		 * @throws InterruptedException
		 */
	
		public void testIsHatWert() throws InterruptedException {
			Speicher s1 = new Speicher();
			s1.setWert(24);
			assertTrue(s1.isHatWert());
		}
		
		/**
		 * 
		 * Tests the method getWert(). Creates a new Speicher instance, inputs a value (24) in the Speicher instance s1 and test if the method getWert returns the right
		 * value.
		 * @throws InterruptedException
		 */
		
		public void testGetWert() throws InterruptedException {
			Speicher s1 = new Speicher();
			s1.setWert(24);
			assertEquals(s1.getWert(), 24);
		}
}
