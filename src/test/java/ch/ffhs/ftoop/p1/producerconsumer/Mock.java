package ch.ffhs.ftoop.p1.producerconsumer;

import java.util.LinkedList;

/**
 * This is a mock class to test the Zaehler class. It inheritants from the Speicher class. It's purpose its to safe inputs from a Zaehler instance into a   
 * List, to check if the right values get safed into this class.
 * 
 *
 */

public class Mock extends Speicher{
	public LinkedList<Integer> list = new LinkedList<>();
	
	/**
	 * Adds a value to the linked list.
	 */
	
	@Override
	public void setWert(int wert) throws InterruptedException {
		list.add(wert);
	}
}
