package ch.ffhs.ftoop.p1.producerconsumer;

import java.util.Arrays;
import java.util.LinkedList;

import student.TestCase;

/**
 * This class tests instances from the Zaehler class. 
 * 
 */

public class ZaehlerTest extends TestCase {
	
	/**
	 * This method tests instances from the Zaehler class. It generates a new instance of the mock class which functions as a replacement class for the Speicher class 
	 * for test purposes. It then generates a new instance of the Zaehler class and assigns the mock object s1 and the two values 2 (min) and 10 (max). After that we test 
	 * if the safed inputs in the mock class equals the ectual values, which we harcoded in to this method (LinkedList "list").
	 * 
	 * @throws InterruptedException
	 */
	
	public void testZaehlerSave() throws InterruptedException {
		Mock s1 = new Mock();
		Zaehler z1 = new Zaehler(s1, 2, 10);
		Integer[] array = {2,3,4,5,6,7,8,9,10};
		LinkedList<Integer> list= new LinkedList<>(Arrays.asList(array));
		z1.start();
		Thread.sleep(2000);
		assertEquals(list, s1.list);
	}
}
